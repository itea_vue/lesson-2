
Vue.component(
    'GlobalBtn',
    {
        data() {
            return {
                someNum: 0
            }
        },
        methods: {
            clickHandler() {
                console.log('[global component]');
                this.someNum ++
            }
        },
        template: `
            <button @click="clickHandler">Global btn ({{ someNum }})</button>
        `
    }
);