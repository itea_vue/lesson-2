
const Guests = {
    components: {
        GuestItem
    },
    data() {
        return {
            guests: []
        }
    },
    created() {
        this.guests = guestsList.map(item => {
            item.isPresent = false;
            return item
        });
    },
    methods: {
        presenceToggler(id) {
            this.guests = this.guests.map(item => {
                if(item._id === id) {
                    item.isPresent = !item.isPresent
                }
                return item
            })
        }
    },
    template: `
        <div class="guests">
            <ol>
                <guest-item v-for="guest in guests" :key="guest._id" :guest="guest" @present-tog="presenceToggler"></guest-item>
            </ol>
        </div>
    `
}