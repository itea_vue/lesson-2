const LocaleBtn = {
    // props - как массив
    // props: ['label', 'inputType'],

    // props -  как обьект
    props: {
        label: {
            type: String,
            default: 'btn',
            /*
            * default: () => ({}) - для обьектов и массивов должна возвращаться функция которая возвращает обьект или массив
            * */
            // required: true, // false
            // validator: val => val
        },
        inputType: [String, Number]
    },
    /*
    * String
    * Number
    * Function
    * Object
    * Array
    * Promise
    * Symbol
    * Boolean
    * */
    data() {
        return {
            fInput: '',
            output: '',
            dLabel: this.label
        }
    },
    computed: {
        adStr() {
            return this.label + 'sdfsd'
        }
    },

    // Хуки жизненного цикла (*не все)
    // beforeCreate() {
    //     // console.log('[beforeCreate]', this);
    //     // debugger;
    // },
    // created() {
    //     // console.log('[created]', this);
    //     // debugger;
    //     this.output = 'test with created';
    // },
    // beforeDestroy() {
    //     console.log('[beforeDestroy]', this);
    //     debugger
    // },

    methods: {
        onClick(e) {
            this.output = this.fInput;
            console.log('label', this.label);
            console.log('inputType', this.inputType);
            this.$emit('my-event', this.fInput, e);
        }
    },
    template: `
        <div>
            <input :type="inputType" v-model="fInput">
            <button @click="onClick($event)">{{ adStr }}</button>
            <div>{{ output }}</div>
        </div>
    `
};